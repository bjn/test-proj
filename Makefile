all: hello

hello: hello.c
	$(CC) -o $@ $<

test: hello
	./hello




writerootfile.o: writerootfile.C
	$(CXX) -o $@ -c $< `root-config --cflags`

writerootfile: writerootfile.o
	$(CXX) -o $@ $< `root-config --ldflags --libs`

testrootfile: writerootfile
	./writerootfile
	ls -l *.root
